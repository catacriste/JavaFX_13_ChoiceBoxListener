package application;
	
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
/*ChoiceBox cont�m as op��es a escolher
 * Listener executa a a��o dispensado o bot�o*/

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
		
			
			ChoiceBox<String> choiceBox = new ChoiceBox<>();
			
			choiceBox.getItems().add("Op��o 1");
			choiceBox.getItems().add("Op��o 2");
			choiceBox.getItems().add("Op��o 3");
			choiceBox.getItems().addAll("Op��o 4", "Op��o 5");
			
			choiceBox.setValue("Op��o 3");
			
			//Listener - capta uma altera��o na sele��o
			choiceBox.getSelectionModel().selectedItemProperty().addListener( (v,oldValue,newValue) -> {
				Utils.alertBox("Sele��o", "Op��o Selecionada\n" + choiceBox.getValue());
			});
			
			/*- getSelectionModel - define o m�todo de sele��o dos itens, h� varios :
			 * 		- selectNext. selectPrevious, selectedIndexProprety, etc
			 * 		- selectedItemProprety - permite apenas uma sele��o por objeto
			 * - addListener ativa o detetor de eventos para altera��es no itema
			 * 	-express�o LAMBDA com 3 parametros entrada, dados por selectedItemProperty(v, oldValue , newValue)
			 * 			-v � a lista de op��es
			 * 			-oldValue � item que estava selecionado
			 * 			- newValue � o novo item selecionado*/
			
			
			
			VBox layoutChoiceBox = new VBox(10);
			layoutChoiceBox.setPadding(new Insets(20,20,20,20));
			layoutChoiceBox.getChildren().addAll(choiceBox);
			
			
			
			BorderPane root = new BorderPane();
			Scene scene = new Scene(layoutChoiceBox,300,200);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}